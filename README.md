# Piece

Instant Messaging play interpretor

## How?

Edit `main.js` with your piece json file.

More options and demo here soon.

## TODO

- IM System messages should be different than Piece’s system messages e.g. “End of piece”
- Sound tests
- Unsplit function can be deduced with the presence of `frame` key
- Navigation between Pieces

## Tips

- Special character name `system` will be styled as a system / bot
- The piece can loop forever: add a `"loop":true,` in the piece’s json (after `"author"` for exemple)
- `hide-` before a piece’s filename hides it from the selector menu
- HTML is supported in messages. e.g. `<img src='assets/img.png'>` will work

# Demo

Soon

## Credits

[Raphaël Bastide](http://raphaelbastide.com/)

## License

[GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl-3.0.en.html)
