/**
 * Timed
 * Copyright (c) 2011 Jarvis Badgley, Arthur Klepchukov
 * https://github.com/ChiperSoft/Timed
 * Licensed under the BSD license (BSD_LICENSE.txt)
 *
 * @author <a href="mailto:chipersoft@gmail.com">Jarvis Badgley</a>
 * @version 1.3.1
 */
(function(d){function f(){var a={when:null,callback:null,cancel:g,start:h},c=arguments.length;a.callback=arguments[c-1];if(typeof a.callback!=="function")throw"Timed.after and Timed.every - Require a callback as the last argument";var b=(arguments[0]instanceof Array?arguments[0].join(""):String(arguments[0]))+(c===3?String(arguments[1]):"");isNaN(parseInt(b,10))&&(b="1"+b);b=b.toLowerCase().replace(/[^a-z0-9\.]/g,"").match(/(?:(\d+(?:\.\d+)?)(?:days?|d))?(?:(\d+(?:\.\d+)?)(?:hours?|hrs?|h))?(?:(\d+(?:\.\d+)?)(?:minutes?|mins?|m))?(?:(\d+(?:\.\d+)?)(?:seconds?|secs?|s))?(?:(\d+(?:\.\d+)?)(?:milliseconds?|ms))?/);
if(b[0])a.when=parseFloat(b[1]||0)*864E5+parseFloat(b[2]||0)*36E5+parseFloat(b[3]||0)*6E4+parseFloat(b[4]||0)*1E3+parseInt(b[5]||0,10);else if(c===3||!(a.when=parseInt(arguments[0],10)))throw"Timed.after and Timed.every - Could not parse delay arguments, check your syntax";return a}var g=function(){if(this.id&&clearInterval(this.id))this.id=null;return this},h=function(){this.id=this.type=="i"?setInterval(this.callback,this.when):setTimeout(this.callback,this.when);return this},e={after:function(){var a=
f.apply(this,arguments);a.type="t";return a.start()},every:function(){var a=f.apply(this,arguments);a.type="i";return a.start()},yield:function(a){if(window.postMessage&&window.addEventListener){var c,b=Math.round(Math.random()*1E6);window.addEventListener("message",c=function(d){d.data===b&&(window.removeEventListener("message",c),a())});window.postMessage(b,"*")}else e.after(0,"ms",a)}},i=d.Timed;e.noConflict=function(){d.Timed=i;return this};d.Timed=e})(this);
