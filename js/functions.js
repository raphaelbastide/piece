
// Custom Functions used in pieces

// Split func
function split(parts, toggle){
  if (toggle) {
    setTimeout(function(){
      $(".content").addClass('split');
    }, 0);
  }else {
    $(".content").removeClass('split');
  }
}

// Test func
function test(state, string){
  if (state) {
    console.log(string);
  }else {
    console.log("test = false");
  }
}

// AddClass func
function lineClass(line, cl){
  var line = $('#id-'+line);
  setTimeout(function(){
    line.addClass(cl);
  }, 1000);
}

// mainClass func
function mainClass(line, cl){
  setTimeout(function(){
    $(".content").addClass(cl);
  }, 100);
}
