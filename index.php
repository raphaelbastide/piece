<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset=utf-8 />
    <title>Piece</title>
    <link rel="icon" type="image/png" href="img/favicon.png">
    <meta property="og:image" content="http://raphaelbastide.com/piece/img/icon.png" />
    <link rel="stylesheet" type="text/css" href="css/main.css" />
  </head>
  <?php
  $nopiece = true;
  if (isset($_GET["p"]) && ("" !== $_GET["p"])) {
    $nopiece = false;
    $current_piece_file = urldecode($_GET["p"]);
    $current_piece_path = "pieces/".$current_piece_file;
    echo "<script>var piece = 'pieces/$current_piece_file';</script>";
  }
  if ($nopiece) {
    echo "<body class='nopiece'>";
  }else {
    echo "<body>";
  }
   ?>
    <header>
      <form class="selector" action="/" method="get">
        <select name="p" onchange="this.form.submit()">
          <option value=""></option>
          <?php


          $piece_dir = "pieces/";
          $pieces = glob($piece_dir."{*.json}", GLOB_BRACE);
          foreach ($pieces as $piece) {
            $file = basename($piece);
            $content = file_get_contents($piece);
            if (substr( $file, 0, 5 ) !== "hide-") {
              if ($content != "") {
                $title = json_decode($content)->{"title"};
                $author = json_decode($content)->{"author"};
                echo "<option value='$file'";
                if (!$nopiece && $file === $current_piece_file) {
                  echo "selected";
                }
                echo ">$title by $author</option>";
              }else {
                echo "<b>Piece undefined<b>";
              }
            }
          }
          ?>
        </select>
      </form>
    </header>
    <section class="content">
      <footer></footer>
    </section>
    <footer>
      <div class="credits">
        <a href="https://gitlab.com/raphaelbastide/piece/blob/master/README.md">About Piece</a>
      </div>
    </footer>
  </body>
  <script type="text/javascript" src="js/jquery-3.1.0.min.js"></script>
  <script type="text/javascript" src="js/timed.js"></script>
  <script type="text/javascript" src="js/visible.js"></script>
  <script type="text/javascript" src="js/slug.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
  <script type="text/javascript" src="js/functions.js"></script>
</html>
